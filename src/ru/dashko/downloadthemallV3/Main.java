package ru.dashko.downloadthemallV3;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

    private static final String IN_FILE = "src\\ru\\dashko\\downloadthemallV3\\inFile.txt";

    public static void main(String[] args) {

        /*
          Будет содержать пути к музыке
         */
        ArrayList<String> musicFilesPaths = new ArrayList<>();
        /*
          Будет содержать потоки, скачивающие музыку
         */
        ArrayList<DownloadThread> musicThreads = new ArrayList<>();

        try (BufferedReader inFileReader = new BufferedReader(new FileReader(IN_FILE))) {
            String string;
            while ((string = inFileReader.readLine()) != null) {
                String[] linkAndPath = string.split(" ");
                DownloadThread downloadThread = new DownloadThread(linkAndPath[0], linkAndPath[1]);
                if (linkAndPath[1].contains(".mp3")) {
                    musicThreads.add(downloadThread);
                    musicFilesPaths.add(linkAndPath[1]);
                }
                downloadThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        waitCompleteDownload(musicThreads);
        playAll(musicFilesPaths);
    }

    /**
     * Ожидает завершения потоков, скачивающих файлы
     *
     * @param threads ArrayList, содержащий потоки скачивания, завершения которых мы собираемся ждать
     */
    private static void waitCompleteDownload(ArrayList<DownloadThread> threads) {
        for (DownloadThread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Воспроизводит все композиции, пути к которым содержатся в ArrayList'е
     *
     * @param music ArrayList, содержащий пути к .mp3 файлам
     */
    private static void playAll(ArrayList<String> music) {
        for (String aMusic : music) {
            System.out.println("Сейчас играет " + aMusic);
            playMusic(aMusic);
        }
    }


    /**
     * Воспроизводит композицию по указанному пути
     *
     * @param pathToMusic путь к музыкальному файлу
     */
    private static void playMusic(String pathToMusic) {
        try (FileInputStream inputStream = new FileInputStream(pathToMusic)) {
            Player player = new Player(inputStream);
            player.play();
        } catch (JavaLayerException | IOException e) {
            e.printStackTrace();
        }
    }
}
