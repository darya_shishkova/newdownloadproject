package ru.dashko.downloadthemallV3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class DownloadThread extends Thread {

    private static final String ERROR_MSG = "Файл %s не может быть загружен \n";
    private static final String INCORRECT_PATH = "Ошибка! Не удалось создать файл по указанному пути";
    private static final String INCORRECT_URL = "Ошибка! Некорректный URL адрес";
    private static final String SUCCESS = "Файл %s успешно загружен! \n";

    /*
      Ссылка на скачивание файла
     */
    private String downloadUrl;
    /*
      Путь для сохранения файла
     */
    private String saveTo;

    /**
     * Конструктор потока скачивания
     *
     * @param downloadUrl ссылка на скачивание файла
     * @param saveTo      путь, куда будем сохранять файл
     */
    DownloadThread(String downloadUrl, String saveTo) {
        this.downloadUrl = downloadUrl;
        this.saveTo = saveTo;
    }

    /**
     * Запускает поток скачивания
     */
    @Override
    public void run() {
        downloadUsingNIO(downloadUrl, saveTo);
    }

    /**
     * Метод выполняющий загрузку файлов
     *
     * @param downloadUrl ссылка на скачивание
     * @param path        путь к будущему файлу
     */
    private static void downloadUsingNIO(String downloadUrl, String path) {
        try {
            URL url = new URL(downloadUrl);
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(path);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
            System.out.printf(SUCCESS, path);
        } catch (FileNotFoundException e) {
            System.out.println(INCORRECT_PATH);
            System.out.printf(ERROR_MSG, path);
        } catch (MalformedURLException e) {
            System.out.println(INCORRECT_URL);
            System.out.printf(ERROR_MSG, path);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.printf(ERROR_MSG, path);
        }
    }
}
